package mak1soft.visitor;

/**
 * Created by mak1 on 2017-03-26.
 */
public class Fruit implements Item {
    private int price;

    public Fruit(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public int accept(ShoppingCartVisitor visitor) {
        return visitor.visit(this);
    }
}
