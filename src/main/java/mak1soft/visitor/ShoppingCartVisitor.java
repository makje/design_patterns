package mak1soft.visitor;

/**
 * Created by mak1 on 2017-03-26.
 */
public interface ShoppingCartVisitor {

    int visit(Book book);
    int visit(Fruit fruit);
}
