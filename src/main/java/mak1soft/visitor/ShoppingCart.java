package mak1soft.visitor;

/**
 * Created by mak1 on 2017-03-26.
 */
public class ShoppingCart {
    private Item [] items;

    public ShoppingCart(Item[] items) {
        this.items = items;
    }

    public int calculatePrice() {
        ShoppingCartVisitor visitor = new ShoppingCartVisitorImpl();
        int sum = 0;

        for (Item item : items)
            sum = sum + item.accept(visitor);

        return sum;
    }
}
