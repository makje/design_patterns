package mak1soft.visitor;

/**
 * Created by mak1 on 2017-03-26.
 */
public interface Item {
    int accept(ShoppingCartVisitor visitor);
}
