package mak1soft.visitor;

/**
 * Created by mak1 on 2017-03-26.
 */
public class ShoppingCartVisitorImpl implements ShoppingCartVisitor {
    public int visit(Book book) {
        return book.getPrice();
    }

    public int visit(Fruit fruit) {
        return fruit.getPrice();
    }
}
