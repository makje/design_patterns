package mak1soft.builder;

/**
 * Created by mak1 on 2017-03-14.
 */
public interface Item {
    public String name();
    public Packing packing();
    public float price();
}
