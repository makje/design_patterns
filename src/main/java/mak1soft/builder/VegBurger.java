package mak1soft.builder;

/**
 * Created by mak1 on 2017-03-14.
 */
public class VegBurger extends Burger {
    public String name() {
        return "Veg Burger";
    }

    public float price() {
        return 25.0f;
    }
}
