package mak1soft.builder;

/**
 * Created by mak1 on 2017-03-14.
 */
public class Pepsi extends ColdDrink {
    public String name() {
        return "Pepsi";
    }

    public float price() {
        return 35.0f;
    }
}
