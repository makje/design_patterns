package mak1soft.builder;

/**
 * Created by mak1 on 2017-03-14.
 */
public class ChickenBurger extends Burger {
    public String name() {
        return "Chicken Burger";
    }

    public float price() {
        return 50.5f;
    }
}
