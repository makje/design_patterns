package mak1soft.builder;

/**
 * Created by mak1 on 2017-03-14.
 */
public abstract class Burger implements Item {
    public Packing packing() {
        return new Wrapper();
    }

    public abstract float price();
}
