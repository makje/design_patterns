package mak1soft.builder;

/**
 * Created by mak1 on 2017-03-14.
 */
public class Coke extends ColdDrink {
    public String name() {
        return "Coke";
    }

    public float price() {
        return 30.0f;
    }
}
