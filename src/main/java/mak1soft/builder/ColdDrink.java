package mak1soft.builder;

/**
 * Created by mak1 on 2017-03-14.
 */
public abstract class ColdDrink implements Item {
    public Packing packing() {
        return new Bottle();
    }

    public abstract float price();
}
