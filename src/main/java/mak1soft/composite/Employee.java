package mak1soft.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mak1 on 2017-03-20.
 */
public class Employee {
    private String name;
    private List<Employee> subordinates;

    public Employee(String name) {
        this.name = name;
        subordinates = new ArrayList<Employee>();
    }

    public void add(Employee employee) {
        subordinates.add(employee);
    }

    public void remove(Employee employee) {
        subordinates.remove(employee);
    }

    public List<Employee> getSubordinates() {
        return subordinates;
    }

    public String toString() {
        return "employee: " + name;
    }
}
