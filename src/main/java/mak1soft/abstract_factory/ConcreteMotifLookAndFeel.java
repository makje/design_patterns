package mak1soft.abstract_factory;

/**
 * Created by mak1 on 2017-03-14.
 */
public class ConcreteMotifLookAndFeel extends AbstractLookAndFeel {

    AbstractButton createButton() {
        return new ConcreteeMotifButton();
    }

    AbstractEditBox createTextField() {
        return new ConcreteMotifEditBox();
    }
}
