package mak1soft.abstract_factory;

/**
 * Created by mak1 on 2017-03-14.
 */
public class ConcreteMotifEditBox extends AbstractEditBox {

    public char write() {
        return 'M';
    }
}
