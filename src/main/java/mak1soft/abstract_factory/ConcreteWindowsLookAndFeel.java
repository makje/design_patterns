package mak1soft.abstract_factory;

/**
 * Created by mak1 on 2017-03-14.
 */
public class ConcreteWindowsLookAndFeel extends AbstractLookAndFeel {

    AbstractButton createButton() {
        return new ConcreteWindowsButton();
    }

    AbstractEditBox createTextField() {
        return new ConcreteWindowsEditBox();
    }
}
