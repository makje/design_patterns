package mak1soft.abstract_factory;

/**
 * Created by mak1 on 2017-03-14.
 */
public class LookAndFeelMaker {

    private static AbstractLookAndFeel lookAndFeel = null;

    static AbstractLookAndFeel getLookAndFeel(String choice) {
        if (choice.equals("windows"))
            lookAndFeel = new ConcreteWindowsLookAndFeel();
        else if (choice.equals("motif"))
            lookAndFeel = new ConcreteMotifLookAndFeel();
        else
            lookAndFeel = null;

        return lookAndFeel;
    }
}
