package mak1soft.abstract_factory;

/**
 * Created by mak1 on 2017-03-14.
 */
public class ConcreteWindowsButton extends AbstractButton {

    public int click() {
        return 1;
    }
}
