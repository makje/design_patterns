package mak1soft.abstract_factory;

/**
 * Created by mak1 on 2017-03-14.
 */
abstract class AbstractEditBox {

    public abstract char write();

}
