package mak1soft.object_pool_new;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Created by mak1 on 2017-03-27.
 */
public class ObjectPool<E> {
    private Map<String, E> objectPool = null;
    private Stack<String> unusedObjectKeys = null;

    public ObjectPool() {
        objectPool = new HashMap<String, E>();
        unusedObjectKeys = new Stack<String>();
    }

    public void initializePool(E object) {
        if (!unusedObjectKeys.contains(object.toString())) {
            objectPool.put(object.toString(), object);
            unusedObjectKeys.push(object.toString());
        }
    }

    public Object borrowObject() {
        Object object = null;

        synchronized (this) {
            if (unusedObjectKeys.isEmpty()) {
                try {
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            try {
                object = objectPool.get(unusedObjectKeys.pop());
            } catch (EmptyStackException e) {

            }
        }
        return object;
    }

    public void returnObject(E object) {
        synchronized (this) {
            if (objectPool.containsKey(object.toString())) {
                if (!unusedObjectKeys.contains(object.toString()))
                    unusedObjectKeys.push(object.toString());
                this.notifyAll();
            }
        }
    }

    public void clearPool() {
        objectPool.clear();
        unusedObjectKeys.clear();
    }
}
