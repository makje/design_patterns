package mak1soft.null_object;

/**
 * Created by mak1 on 2017-03-19.
 */
public interface Animal {
    void makeSound();
}
