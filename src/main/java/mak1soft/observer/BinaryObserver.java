package mak1soft.observer;

/**
 * Created by mak1 on 2017-03-19.
 */
public class BinaryObserver extends Observer {

    public BinaryObserver(Subject subject) {
        this.subject = subject;
        this.subject.attach(this);
    }

    public void update() {
        System.out.println(Integer.toBinaryString(subject.getState()));
    }
}
