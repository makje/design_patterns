package mak1soft.observer;

/**
 * Created by mak1 on 2017-03-19.
 */
public abstract class Observer {
    protected Subject subject;
    public abstract void update();
}
