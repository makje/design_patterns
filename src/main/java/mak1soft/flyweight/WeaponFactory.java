package mak1soft.flyweight;

import java.util.HashMap;

/**
 * Created by mak1 on 2017-03-20.
 */
public class WeaponFactory {
    private static final HashMap<String, Weapon> weaponMap = new HashMap();

    public static Weapon getWeapon(String name) {
        Ak47 ak47 = (Ak47) weaponMap.get(name);

        if (ak47 == null) {
            ak47 = new Ak47(name);
            weaponMap.put(name, ak47);
            System.out.print("creating new weapon: " + name);
        }
        return ak47;
    }
}
