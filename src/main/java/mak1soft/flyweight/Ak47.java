package mak1soft.flyweight;

/**
 * Created by mak1 on 2017-03-20.
 */
public class Ak47 implements Weapon {
    private String name;

    public Ak47(String name) {
        this.name = name;
    }

    public void fire() {
        System.out.print(" * ");
    }
}
