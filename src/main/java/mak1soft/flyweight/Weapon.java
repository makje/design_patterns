package mak1soft.flyweight;

/**
 * Created by mak1 on 2017-03-20.
 */
public interface Weapon {
    void fire();
}
