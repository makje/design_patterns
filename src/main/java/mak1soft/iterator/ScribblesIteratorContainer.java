package mak1soft.iterator;

/**
 * Created by mak1 on 2017-03-16.
 */
public interface ScribblesIteratorContainer {

    ScribblesIterator createIterator();
}
