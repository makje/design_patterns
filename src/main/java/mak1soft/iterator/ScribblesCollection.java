package mak1soft.iterator;

/**
 * Created by mak1 on 2017-03-16.
 */
public class ScribblesCollection implements ScribblesIteratorContainer{
    //hardcode
    private String scribbles[] = {"fdsklsn","ksdgfjslkjgsl","gdklsgjs","mdklsggdd",
    "dkslgjdklsg","dskjgseda34","ewq9riwfo","93irmssff","fks21q023","dfskao39"," ","mak1 xD"};

    public ScribblesIterator createIterator() {

        ScribbleIterator scribbleIterator = new ScribbleIterator();
        return scribbleIterator;
    }

    private class ScribbleIterator implements ScribblesIterator {
        private int position;

        public boolean hasNext() {
            if (position < scribbles.length)
                return true;
            else
                return false;
        }

        public Object next() {
            if (this.hasNext())
                return scribbles[position++];
            else
                return null;
        }
    }
}
