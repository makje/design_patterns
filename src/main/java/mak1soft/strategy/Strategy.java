package mak1soft.strategy;

/**
 * Created by mak1 on 2017-03-19.
 */
public interface Strategy {
    int doOperation(int num1, int num2);
}
