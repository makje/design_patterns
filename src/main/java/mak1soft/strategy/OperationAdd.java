package mak1soft.strategy;

/**
 * Created by mak1 on 2017-03-19.
 */
public class OperationAdd implements Strategy {
    public int doOperation(int num1, int num2) {
        return num1 + num2;
    }
}
