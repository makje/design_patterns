package mak1soft.factory_method;

/**
 * Created by mak1 on 2017-03-13.
 */
public abstract class Creator {

    public Animal createAnimal() {
        return factoryMethod();
    }

    protected abstract Animal factoryMethod();
}
