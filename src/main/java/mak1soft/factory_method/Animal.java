package mak1soft.factory_method;

/**
 * Created by mak1 on 2017-03-13.
 */
public interface Animal {

    String voice();
}
