package mak1soft.chain_of_responsibility;

/**
 * Created by mak1 on 2017-03-16.
 */
public class FileLogger extends AbstractLogger {

    public FileLogger(int level) {
        this.level = level;
    }

    protected void write(String message) {
        System.out.println("file logger " + message);
    }
}
