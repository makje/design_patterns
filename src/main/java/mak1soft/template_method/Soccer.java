package mak1soft.template_method;

/**
 * Created by mak1 on 2017-03-19.
 */
public class Soccer extends Game {
    void initialize() {
        System.out.println("soccer initilized");
    }

    void startPlay() {
        System.out.println("soccer started");
    }

    void endPlay() {
        System.out.println("soccer finished");
    }
}
