package mak1soft.template_method;

/**
 * Created by mak1 on 2017-03-19.
 */
public abstract class Game {
    abstract void initialize();
    abstract void startPlay();
    abstract void endPlay();

    //template method
    public final void play() {
        initialize();
        startPlay();
        endPlay();
    }
}
