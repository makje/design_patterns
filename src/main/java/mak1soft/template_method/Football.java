package mak1soft.template_method;

/**
 * Created by mak1 on 2017-03-19.
 */
public class Football extends Game {
    void initialize() {
        System.out.println("football initilized");
    }

    void startPlay() {
        System.out.println("football started");
    }

    void endPlay() {
        System.out.println("football finished");
    }
}
