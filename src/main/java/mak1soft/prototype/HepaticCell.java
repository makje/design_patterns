package mak1soft.prototype;

/**
 * Created by mak1 on 2017-03-14.
 */
public class HepaticCell extends Cell {
    private int number;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

}
