package mak1soft.prototype;

/**
 * Created by mak1 on 2017-03-14.
 */
abstract class Cell implements Cloneable{
    public Object clone() {
        Object clone = null;

        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }
}
