package mak1soft.decorator;

/**
 * Created by mak1 on 2017-03-20.
 */
public interface Shape {
    void draw();
}
