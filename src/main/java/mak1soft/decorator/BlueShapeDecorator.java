package mak1soft.decorator;

/**
 * Created by mak1 on 2017-03-20.
 */
public class BlueShapeDecorator extends ShapeDecorator {

    public BlueShapeDecorator(Shape decoratedShape) {
        super(decoratedShape);
    }

    public void draw() {
        decoratedShape.draw();
        setColorBlue(decoratedShape);
    }

    private void setColorBlue(Shape decoratedShape) {
        System.out.print(" blue");
    }
}
