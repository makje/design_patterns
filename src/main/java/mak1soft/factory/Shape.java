package mak1soft.factory;

/**
 * Created by mak1 on 2017-03-13.
 */
public interface Shape {
    String draw();
}
