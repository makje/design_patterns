package mak1soft.factory;

/**
 * Created by mak1 on 2017-03-13.
 */
public class ShapeFactory {

    public Shape getShape(String shapeType) {
        if (shapeType.equalsIgnoreCase("circle"))
            return new Circle();
        else if (shapeType.equalsIgnoreCase("square"))
            return new Square();
        else
            return null;
    }

}
