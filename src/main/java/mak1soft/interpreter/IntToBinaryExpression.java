package mak1soft.interpreter;

/**
 * Created by mak1 on 2017-03-16.
 */
public class IntToBinaryExpression implements Expression {
    private int i;

    public IntToBinaryExpression(int i){
        this.i = i;
    }

    public String interpret(InterpreterContext interpreterContext) {
        return interpreterContext.getBinaryFormat(this.i);
    }
}
