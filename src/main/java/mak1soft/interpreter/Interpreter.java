package mak1soft.interpreter;

/**
 * Created by mak1 on 2017-03-16.
 */
public class Interpreter {
    private InterpreterContext interpreterContext;

    public Interpreter(InterpreterContext interpreterContext){
        this.interpreterContext = interpreterContext;
    }

    public String construe(String text){
        Expression expression;

        if(text.contains("hex")){
            expression = new IntToHexExpression(Integer.parseInt
                    (text.substring(0, text.indexOf(" "))));
        }else if(text.contains("bin")){
            expression = new IntToBinaryExpression(Integer.parseInt
                    (text.substring(0, text.indexOf(" "))));
        }else {
            return text;
        }

        return expression.interpret(interpreterContext);
    }
}
