package mak1soft.interpreter;

/**
 * Created by mak1 on 2017-03-16.
 */
public class IntToHexExpression implements Expression {
    private int i;

    public IntToHexExpression(int i){
        this.i = i;
    }

    public String interpret(InterpreterContext interpreterContext) {
        return interpreterContext.getHexadecimalFormat(this.i);
    }
}
