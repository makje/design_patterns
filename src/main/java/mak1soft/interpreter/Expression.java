package mak1soft.interpreter;

/**
 * Created by mak1 on 2017-03-16.
 */
public interface Expression {

    String interpret(InterpreterContext interpreterContext);
}
