package mak1soft.memento;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mak1 on 2017-03-19.
 */
public class CareTaker {
    private List<Memento> mementoList = new ArrayList<Memento>();

    public void add(Memento state) {
        mementoList.add(state);
    }

    public Memento get(int index) {
        return mementoList.get(index);
    }
}
