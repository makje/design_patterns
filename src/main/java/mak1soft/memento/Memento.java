package mak1soft.memento;

/**
 * Created by mak1 on 2017-03-19.
 */
public class Memento {
    private String state;

    public Memento(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
