package mak1soft.bridge;

/**
 * Created by mak1 on 2017-03-20.
 */
public class RedCircle implements DrawAPI {
    public void drawCircle(int radius) {
        System.out.println("red circle radius: " + radius);
    }
}
