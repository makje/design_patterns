package mak1soft.bridge;

/**
 * Created by mak1 on 2017-03-20.
 */
public class Circle extends Shape {
    private int radius;

    public Circle(int radius, DrawAPI drawAPI) {
        super(drawAPI);
        this.radius = radius;
    }

    public void draw() {
        drawAPI.drawCircle(radius);
    }
}
