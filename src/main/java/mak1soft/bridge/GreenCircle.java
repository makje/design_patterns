package mak1soft.bridge;

/**
 * Created by mak1 on 2017-03-20.
 */
public class GreenCircle implements DrawAPI {
    public void drawCircle(int radius) {
        System.out.println("green circle radius: " + radius);
    }
}
