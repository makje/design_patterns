package mak1soft.bridge;

/**
 * Created by mak1 on 2017-03-20.
 */
public interface DrawAPI {
    void drawCircle(int radius);
}
