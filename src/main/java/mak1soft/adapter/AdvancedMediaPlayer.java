package mak1soft.adapter;

/**
 * Created by mak1 on 2017-03-20.
 */
public interface AdvancedMediaPlayer {
    void playVlc(String fileName);
    void playMp4(String filename);
}
