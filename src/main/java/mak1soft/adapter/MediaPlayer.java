package mak1soft.adapter;

/**
 * Created by mak1 on 2017-03-20.
 */
public interface MediaPlayer {
    void play(String audioType, String fileName);
}
