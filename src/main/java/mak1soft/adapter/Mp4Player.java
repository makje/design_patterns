package mak1soft.adapter;

/**
 * Created by mak1 on 2017-03-20.
 */
public class Mp4Player implements AdvancedMediaPlayer {
    public void playVlc(String fileName) {
        //empty
    }

    public void playMp4(String filename) {
        System.out.println("playing mp4 file: " + filename);
    }
}
