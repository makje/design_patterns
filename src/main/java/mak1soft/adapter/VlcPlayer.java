package mak1soft.adapter;

import mak1soft.adapter.AdvancedMediaPlayer;

/**
 * Created by mak1 on 2017-03-20.
 */
public class VlcPlayer implements AdvancedMediaPlayer {
    public void playVlc(String fileName) {
        System.out.println("playing vlc file: " + fileName);
    }

    public void playMp4(String filename) {
        //empty
    }
}
