package mak1soft.proxy;

/**
 * Created by mak1 on 2017-03-20.
 */
public interface Image {
    void display();
}
