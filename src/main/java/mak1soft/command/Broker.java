package mak1soft.command;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mak1 on 2017-03-16.
 */
public class Broker {
    private List<Order> orderList = new ArrayList<Order>();

    public void takeOrder(Order order) {
        orderList.add(order);
    }

    public void placeOrders() {

        for (Order order: orderList) {
            order.execute();
        }
        orderList.clear();
    }
}
