package mak1soft.command;

/**
 * Created by mak1 on 2017-03-16.
 */
public class Stock {

    public void buy() {
        System.out.println("one debenture bought");
    }

    public void sell() {
        System.out.println("one debenture sold");
    }
}
