package mak1soft.command;

/**
 * Created by mak1 on 2017-03-16.
 */
public interface Order {
    void execute();
}
