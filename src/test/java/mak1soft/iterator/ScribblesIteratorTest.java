package mak1soft.iterator;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-16.
 */
public class ScribblesIteratorTest {

    @Test
    public void scribblesIteratorTest() {
        ScribblesCollection scribblesCollection = new ScribblesCollection();

        ScribblesIterator scribblesIterator = scribblesCollection.createIterator();
        Assert.assertTrue(scribblesIterator.hasNext());
        while (scribblesIterator.hasNext()) {
            Assert.assertNotNull(scribblesIterator.next());
        }
        Assert.assertFalse(scribblesIterator.hasNext());
        Assert.assertNull(scribblesIterator.next());
    }

    @Test
    public void counterTest() {
        ScribblesCollection scribblesCollection = new ScribblesCollection();
        int counter = 0;

        ScribblesIterator scribblesIterator = scribblesCollection.createIterator();
        while (scribblesIterator.hasNext()) {
            scribblesIterator.next();
            counter++;
        }
        Assert.assertEquals(12, counter);
    }
}
