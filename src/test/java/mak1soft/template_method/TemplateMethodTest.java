package mak1soft.template_method;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-19.
 */
public class TemplateMethodTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void soccerTest() {
        Game game = new Soccer();
        game.play();
        Assert.assertEquals("soccer initilized\r\nsoccer started\r\nsoccer finished\r\n",
                outContent.toString());
    }

    @Test
    public void footballTest() {
        Game game = new Football();
        game.play();
        Assert.assertEquals("football initilized\r\nfootball started\r\nfootball finished\r\n",
                outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
