package mak1soft.memento;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-19.
 */
public class MementoTest {

    @Test
    public void MementoTest() {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();

        originator.setState("empty");
        originator.setState("low");
        careTaker.add(originator.saveStateToMemento());

        originator.setState("high");
        careTaker.add(originator.saveStateToMemento());

        originator.setState("full");
        Assert.assertEquals("full", originator.getState());

        originator.getStateFromMemento(careTaker.get(0));
        Assert.assertEquals("low", originator.getState());

        originator.getStateFromMemento(careTaker.get(1));
        Assert.assertEquals("high", originator.getState());
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void ExceptionTest() {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();

        originator.setState("empty");
        careTaker.add(originator.saveStateToMemento());

        originator.getStateFromMemento(careTaker.get(0));
        Assert.assertEquals("empty", originator.getState());

        originator.getStateFromMemento(careTaker.get(1));
        Assert.assertEquals("empty", originator.getState());
    }
}
