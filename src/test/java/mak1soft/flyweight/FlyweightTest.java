package mak1soft.flyweight;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-20.
 */
public class FlyweightTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void createWeaponTest() {
        Ak47 ak47 = (Ak47) WeaponFactory.getWeapon("ak47");
        Assert.assertNotNull(ak47);
    }

    @Test
    public void flyweightTest() {
        String name = "ak47";

        for (int i = 0; i < 10; i++) {
            if (i >= 8 )
                name = "ak47revB";
            Ak47 ak47 = (Ak47) WeaponFactory.getWeapon(name);
            ak47.fire();
        }
        Assert.assertEquals(" *  *  *  *  *  *  *  * creating new weapon: ak47revB *  * ",
                outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
