package mak1soft.proxy;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-20.
 */
public class ProxyTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void noProxyTest() {
        Image image = new RealImage("1gb.bin");

        image.display();
        image.display();
        image.display();
        Assert.assertEquals("loading 1gb.bin\r\n" +
                "displaying 1gb.bin\r\n" +
                "displaying 1gb.bin\r\n" +
                "displaying 1gb.bin\r\n", outContent.toString());
    }

    @Test
    public void proxyTest() {
        Image image = new ProxyImage("1gb.bin");

        image.display();
        image.display();
        image.display();
        Assert.assertEquals("loading 1gb.bin\r\n" +
                "displaying 1gb.bin\r\n" +
                "displaying 1gb.bin\r\n" +
                "displaying 1gb.bin\r\n", outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
