package mak1soft.visitor;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-19.
 */
public class VisitorTest {

    @Test
    public void visitorTest() {
        ShoppingCartVisitor visitor = new ShoppingCartVisitorImpl();
        Item item1 = new Book(10);
        Item item2 = new Fruit(1);

        Assert.assertEquals(10, item1.accept(visitor));
        Assert.assertEquals(1, item2.accept(visitor));
    }

    @Test
    public void shoppingCartTest() {
        Item[] items = new Item[]{new Book(10), new Fruit(1), new Fruit(1)};
        ShoppingCart shoppingCart = new ShoppingCart(items);

        Assert.assertEquals(12, shoppingCart.calculatePrice());
    }

}
