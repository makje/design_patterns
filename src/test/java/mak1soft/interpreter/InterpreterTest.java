package mak1soft.interpreter;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-16.
 */
public class InterpreterTest {

    @Test
    public void binTest() {
        String s = "28 bin";

        Interpreter interpreter = new Interpreter(new InterpreterContext());
        interpreter.construe(s);
        Assert.assertEquals("11100", interpreter.construe(s));
    }

    @Test
    public void hexTest() {
        String s = "28 hex";

        Interpreter interpreter = new Interpreter(new InterpreterContext());
        interpreter.construe(s);
        Assert.assertEquals("1c", interpreter.construe(s));
    }

    @Test
    public void notBinOrHexTest() {
        String s = "28";

        Interpreter interpreter = new Interpreter(new InterpreterContext());
        interpreter.construe(s);
        Assert.assertEquals("28", interpreter.construe(s));
    }

    @Test(expected = NumberFormatException.class)
    public void irrelevantText() {
        String s = "30i 3dfd0hex";

        Interpreter interpreter = new Interpreter(new InterpreterContext());
        interpreter.construe(s);
        Assert.assertEquals("fail", interpreter.construe(s));
    }

    @Test
    public void sequenceTest() {
        String s = "28 binhex";

        Interpreter interpreter = new Interpreter(new InterpreterContext());
        interpreter.construe(s);
        Assert.assertEquals("1c", interpreter.construe(s));
    }

}
