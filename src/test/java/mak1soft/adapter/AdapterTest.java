package mak1soft.adapter;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-20.
 */
public class AdapterTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void mp3Test() {
        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("mp3", "mak1.mp3");
        Assert.assertEquals("playing mp3 file: mak1.mp3\r\n", outContent.toString());
    }

    @Test
    public void mp4Test() {
        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("mp4", "mak1.mp4");
        Assert.assertEquals("playing mp4 file: mak1.mp4\r\n", outContent.toString());
    }

    @Test
    public void vlcTest() {
        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("vlc", "mak1.vlc");
        Assert.assertEquals("playing vlc file: mak1.vlc\r\n", outContent.toString());
    }

    @Test
    public void unsuppertedTest() {
        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("avi", "mak1.avi");
        Assert.assertEquals("unsupported file: mak1.avi\r\n", outContent.toString());
    }

    @Test
    public void vlcMediaAdapterTest() {
        MediaAdapter mediaAdapter = new MediaAdapter("vlc");

        mediaAdapter.play("mp4", "mak1.mp4");
        Assert.assertEquals("", outContent.toString());

        mediaAdapter.play("vlc", "mak1.vlc");
        Assert.assertEquals("playing vlc file: mak1.vlc\r\n", outContent.toString());
    }

    @Test
    public void mp4MediaAdapterTest() {
        MediaAdapter mediaAdapter = new MediaAdapter("mp4");

        mediaAdapter.play("vlc", "mak1.vlc");
        Assert.assertEquals("", outContent.toString());

        mediaAdapter.play("mp4", "mak1.mp4");
        Assert.assertEquals("playing mp4 file: mak1.mp4\r\n", outContent.toString());
    }

    @Test
    public void nilMediaAdaperTest() {
        MediaAdapter mediaAdapter = new MediaAdapter("avi");

        mediaAdapter.play("avi", "mak1.avi");
        Assert.assertEquals("", outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
