package mak1soft.startegy;

import mak1soft.strategy.*;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-19.
 */
public class StrategyTest {

    @Test
    public void strategyTest() {
        Context context = new Context(new OperationAdd());
        Strategy substract = new OperationSubstract();

        Assert.assertEquals(12, context.executeStrategy(10, 2));

        context = new Context(substract);
        Assert.assertEquals(8, context.executeStrategy(10, 2));

        context = new Context(new OperationMultiply());
        Assert.assertEquals(20, context.executeStrategy(10, 2));

        context = new Context(substract);
        Assert.assertEquals(-2, context.executeStrategy(10, 12));
    }
}
