package mak1soft.command;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-16.
 */
public class CommandTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void buyTest() {
        Stock stock = new Stock();

        BuyStock buyStock = new BuyStock(stock);

        Broker broker = new Broker();
        broker.takeOrder(buyStock);

        broker.placeOrders();

        Assert.assertEquals("one debenture bought\r\n", outContent.toString());
    }

    @Test
    public void sellTest() {
        Stock stock = new Stock();

        SellStock sellStock = new SellStock(stock);

        Broker broker = new Broker();
        broker.takeOrder(sellStock);

        broker.placeOrders();

        Assert.assertEquals("one debenture sold\r\n", outContent.toString());
    }

    @Test
    public void buySellTest() {
        Stock abcStock = new Stock();

        BuyStock buyStockOrder = new BuyStock(abcStock);
        SellStock sellStockOrder = new SellStock(abcStock);

        Broker broker = new Broker();
        broker.takeOrder(buyStockOrder);
        broker.takeOrder(sellStockOrder);

        broker.placeOrders();

        Assert.assertEquals("one debenture bought\r\none debenture sold\r\n",
                outContent.toString());
    }

    @Test
    public void buyBuySellTest() {
        Stock abcStock = new Stock();

        BuyStock buyStockOrder = new BuyStock(abcStock);
        SellStock sellStockOrder = new SellStock(abcStock);

        Broker broker = new Broker();
        broker.takeOrder(buyStockOrder);
        broker.takeOrder(buyStockOrder);
        broker.takeOrder(sellStockOrder);

        broker.placeOrders();

        Assert.assertEquals("one debenture bought\r\none debenture bought\r\none debenture sold\r\n",
                outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
