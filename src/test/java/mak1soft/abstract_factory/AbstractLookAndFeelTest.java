package mak1soft.abstract_factory;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-14.
 */
public class AbstractLookAndFeelTest {

    @Test
    public void WindowsButtonTest() {
        AbstractLookAndFeel abstractLookAndFeel = LookAndFeelMaker.getLookAndFeel("windows");
        Assert.assertEquals(1, abstractLookAndFeel.createButton().click());
    }

    @Test
    public void MotifButonTest() {
        AbstractLookAndFeel abstractLookAndFeel = LookAndFeelMaker.getLookAndFeel("motif");
        Assert.assertEquals(2, abstractLookAndFeel.createButton().click());

    }

    @Test
    public void WindowsEditBoxTest() {
        AbstractLookAndFeel abstractLookAndFeel = LookAndFeelMaker.getLookAndFeel("windows");
        Assert.assertEquals('W', abstractLookAndFeel.createTextField().write());
    }

    @Test
    public void MotifEditBoxTest() {
        AbstractLookAndFeel abstractLookAndFeel = LookAndFeelMaker.getLookAndFeel("motif");
        Assert.assertEquals('M', abstractLookAndFeel.createTextField().write());
    }

    @Test
    public void nilTest() {
        AbstractLookAndFeel abstractLookAndFeel = LookAndFeelMaker.getLookAndFeel("linux");
        Assert.assertNull(abstractLookAndFeel);
        Assert.assertEquals(null, abstractLookAndFeel.createTextField().write());
    }
}
