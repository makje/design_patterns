package mak1soft.prototype;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-14.
 */
public class CellTest {

    @Test
    public void cardiacCellTest() {
        CardiacCell cardiacCell = new CardiacCell();
        Assert.assertNotNull(cardiacCell);

        CardiacCell cardiacCell1Clone = (CardiacCell) cardiacCell.clone();
        Assert.assertNotNull(cardiacCell1Clone);
    }

    @Test
    public void hepaticCellTest() throws CloneNotSupportedException {
        HepaticCell hepaticCell = new HepaticCell();
        Assert.assertNotNull(hepaticCell);

        HepaticCell hepaticCellCloned1 = (HepaticCell) hepaticCell.clone();
        hepaticCell.setNumber(70);
        HepaticCell hepaticCellCloned2 = (HepaticCell) hepaticCell.clone();

        Assert.assertNotNull(hepaticCellCloned1);
        Assert.assertNotEquals(70, hepaticCellCloned1.getNumber());
        Assert.assertEquals(70, hepaticCellCloned2.getNumber());
    }
}
