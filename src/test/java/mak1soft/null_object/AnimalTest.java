package mak1soft.null_object;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-19.
 */
public class AnimalTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void animalTest() {
        Animal dog = new Dog();
        dog.makeSound();
        Assert.assertEquals("hau!", outContent.toString());
    }

    @Test
    public void nullObjectTest() {
        Animal nullAnimal = new NullAnimal();
        nullAnimal.makeSound();
        Assert.assertEquals("", outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
