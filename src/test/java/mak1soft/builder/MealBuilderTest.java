package mak1soft.builder;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-14.
 */
public class MealBuilderTest {
    MealBuilder mealBuilder = new MealBuilder();

    @Test
    public void buildVegBurgerTest() {
        Meal meal = mealBuilder.buildVegMeal();
        Assert.assertEquals(55.0, meal.getCost(),0.0);
    }

    @Test
    public void buildNonVegBurgerTest() {
        Meal meal = mealBuilder.buildNonVegMeal();
        Assert.assertEquals(85.5, meal.getCost(),0.0);
    }

    @Test
    public void MeatTest() {
        Meal meal = new Meal();

        meal.addItem(new VegBurger());
        Assert.assertEquals(25.0, meal.getCost(),0.0);

        meal.addItem(new Pepsi());
        Assert.assertEquals(60.0, meal.getCost(),0.0);

        meal.addItem(new ChickenBurger());
        Assert.assertEquals(110.5, meal.getCost(),0.0);

        meal.addItem(new Coke());
        Assert.assertEquals(140.5, meal.getCost(), 0.0);
    }
}
