package mak1soft.object_pool_new;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Stack;

/**
 * Created by mak1 on 2017-03-27.
 */
public class ObjectPoolNewTest {
    private ObjectPool<Integer> objectPool = new ObjectPool();
    private Map<String, Integer> objectPoolMap = null;
    private Stack<String> unusedObjectKeysMap = null;

    @Before
    public void setUpFields() {
        try {
            Field objectPoolField = ObjectPool.class.getDeclaredField("objectPool");
            Field unusedObjectKeysField = ObjectPool.class.getDeclaredField("unusedObjectKeys");
            objectPoolField.setAccessible(true);
            unusedObjectKeysField.setAccessible(true);
            objectPoolMap = (Map<String, Integer>) objectPoolField.get(objectPool);
            unusedObjectKeysMap = (Stack<String>) unusedObjectKeysField.get(objectPool);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void ObjectPoolIsEmptyTest() {
        Integer obj = 0;
        Assert.assertEquals(true, unusedObjectKeysMap.isEmpty());
        Assert.assertEquals(true, objectPoolMap.isEmpty());

        objectPool.initializePool(obj);
        Assert.assertEquals(false, unusedObjectKeysMap.isEmpty());
        Assert.assertEquals(false, objectPoolMap.isEmpty());

        objectPool.borrowObject();
        Assert.assertEquals(true, unusedObjectKeysMap.isEmpty());
        Assert.assertEquals(false, objectPoolMap.isEmpty());

        objectPool.returnObject(obj);
        Assert.assertEquals(false, unusedObjectKeysMap.isEmpty());
        Assert.assertEquals(false, objectPoolMap.isEmpty());

        objectPool.clearPool();
        Assert.assertEquals(true, unusedObjectKeysMap.isEmpty());
        Assert.assertEquals(true, objectPoolMap.isEmpty());
    }

    @Test
    public void ObjectPoolSizeTest() {
        Integer obj1 = 0;
        Integer obj2 = 1;

        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(0, objectPoolMap.size());

        objectPool.initializePool(obj1);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.initializePool(obj2);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.borrowObject();
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.borrowObject();
        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.returnObject(obj1);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.returnObject(obj2);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.returnObject(obj1);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.returnObject(obj2);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.clearPool();
        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(0, objectPoolMap.size());

    }

    @Test
    public void sameObjectTest() {
        Integer obj1 = 1;
        Integer obj2 = 1;

        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(0, objectPoolMap.size());

        objectPool.initializePool(obj1);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.initializePool(obj2);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.borrowObject();
        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.returnObject(obj1);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.returnObject(obj2);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.returnObject(obj1);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.returnObject(obj2);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.clearPool();
        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(0, objectPoolMap.size());
    }

    @Test
    public void moreObjectsTest() {
        Integer obj1 = 0;
        Integer obj2 = 1;
        Integer obj3 = 2;
        Integer obj4 = 2;

        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(0, objectPoolMap.size());

        objectPool.initializePool(obj1);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(1, objectPoolMap.size());

        objectPool.initializePool(obj2);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(2, objectPoolMap.size());

        objectPool.initializePool(obj3);
        Assert.assertEquals(3, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.initializePool(obj4);
        Assert.assertEquals(3, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.borrowObject();
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.borrowObject();
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.borrowObject();
        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.returnObject(obj1);
        Assert.assertEquals(1, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.returnObject(obj2);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.returnObject(obj1);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.returnObject(obj2);
        Assert.assertEquals(2, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.returnObject(obj4);
        Assert.assertEquals(3, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.returnObject(obj4);
        Assert.assertEquals(3, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.returnObject(obj3);
        Assert.assertEquals(3, unusedObjectKeysMap.size());
        Assert.assertEquals(3, objectPoolMap.size());

        objectPool.clearPool();
        Assert.assertEquals(0, unusedObjectKeysMap.size());
        Assert.assertEquals(0, objectPoolMap.size());

    }

    @After
    public void cleanUpFields() {
        objectPoolMap = null;
        unusedObjectKeysMap = null;
    }

}