package mak1soft.mediator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-19.
 */
public class MediatorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void sendMessageTest() {
        User popo = new User("popo");
        User mak1 = new User("mak1");

        popo.sendMessage("hi mak1");
        Assert.assertEquals("[popo] : hi mak1\r\n", outContent.toString());
        mak1.sendMessage("hi popo");
        Assert.assertEquals("[popo] : hi mak1\r\n[mak1] : hi popo\r\n", outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
