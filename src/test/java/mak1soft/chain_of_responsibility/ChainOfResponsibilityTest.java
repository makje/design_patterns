package mak1soft.chain_of_responsibility;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-16.
 */
public class ChainOfResponsibilityTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void consoleLoggerTest() {
        AbstractLogger loggerChain = ChainOfResponsibility.getChainOfLoggers();
        loggerChain.logMessage(AbstractLogger.INFO, "info");
        Assert.assertEquals("console logger info\r\n", outContent.toString());
    }

    @Test
    public void debugLoggerTest() {
        AbstractLogger loggerChain = ChainOfResponsibility.getChainOfLoggers();
        loggerChain.logMessage(AbstractLogger.DEBUG, "debug");
        Assert.assertEquals("file logger debug\r\nconsole logger debug\r\n", outContent.toString());
    }

    @Test
    public void errorLoggerTest() {
        AbstractLogger loggerChain = ChainOfResponsibility.getChainOfLoggers();
        loggerChain.logMessage(AbstractLogger.ERROR, "error");
        Assert.assertEquals("error logger error\r\nfile logger error\r\nconsole logger error\r\n",
                outContent.toString());
    }

    @Test
    public void logMessageTest() {
        AbstractLogger loggerChain = ChainOfResponsibility.getChainOfLoggers();
        loggerChain.logMessage(AbstractLogger.ERROR, "1");
        loggerChain.logMessage(AbstractLogger.INFO, "2");
        Assert.assertEquals("error logger 1\r\n" +
                        "file logger 1\r\n" +
                        "console logger 1\r\n" +
                        "console logger 2\r\n",
                outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
