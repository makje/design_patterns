package mak1soft.factory;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-13.
 */
public class ShapeFactoryTest {

    ShapeFactory shapeFactory = new ShapeFactory();
    Shape shape1 = shapeFactory.getShape("circle");
    Shape shape2 = shapeFactory.getShape("square");
    Shape shape3 = shapeFactory.getShape("rectangle");
    Shape shape4 = shapeFactory.getShape("square");

    @Test
    public void isShapeNotNullTest() {
        Assert.assertNotNull(shape1);
        Assert.assertNotNull(shape2);
    }

    @Test
    public void nullShapeTest() {
        Assert.assertNull(shape3);
    }

    @Test
    public void notEqualsShapeTest() {
        Assert.assertNotEquals(shape1.draw(), shape2.draw());
        Assert.assertNotSame(shape1.draw(), shape2.draw());

    }

    @Test
    public void equalsShapeTest() {
        Assert.assertEquals(shape2.draw(), shape4.draw());
        Assert.assertSame(shape2.draw(), shape4.draw());
    }

}
