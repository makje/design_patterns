package mak1soft.bridge;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-20.
 */
public class BridgeTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void redCircleTest() {
        Shape redCircle = new Circle(10, new RedCircle());

        redCircle.draw();
        Assert.assertEquals("red circle radius: 10\r\n", outContent.toString());
    }

    @Test
    public void greenCircleTest() {
        Shape greenCircle = new Circle(20, new GreenCircle());

        greenCircle.draw();
        Assert.assertEquals("green circle radius: 20\r\n", outContent.toString());
    }

    @Test
    public void radiusCircleTest() {
        Shape circle = new Circle(-1, new GreenCircle());

        circle.drawAPI.drawCircle(1);
        Assert.assertEquals("green circle radius: 1\r\n", outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
