package mak1soft.factory_method;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-13.
 */
public class FactoryMethodTest {

    @Test
    public void dogTest() {
        Creator creator = new DogCreator();
        Assert.assertNotNull(creator.createAnimal());
        Assert.assertEquals("hau", creator.factoryMethod().voice());
        Assert.assertEquals("hau", creator.createAnimal().voice());
    }

    @Test
    public void catTest() {
        Creator creator = new CatCreator();
        Assert.assertNotNull(creator.createAnimal());
        Assert.assertEquals("miau", creator.factoryMethod().voice());
        Assert.assertEquals("miau", creator.createAnimal().voice());
    }

}
