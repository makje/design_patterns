package mak1soft.decorator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-20.
 */
public class DecoratorTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void squareTest() {
        Shape square = new Square();

        square.draw();
        Assert.assertEquals("square", outContent.toString());
    }

    @Test
    public void blueSquareTest() {
        Shape bluesquare = new BlueShapeDecorator(new Square());

        bluesquare.draw();
        Assert.assertEquals("square blue", outContent.toString());
    }

    @Test
    public void rectangleTest() {
        Shape rectangle = new Rectangle();

        rectangle.draw();
        Assert.assertEquals("rectangle", outContent.toString());
    }

    @Test
    public void blueRectangleTest() {
        Shape blueRectangle = new BlueShapeDecorator(new Rectangle());

        blueRectangle.draw();
        Assert.assertEquals("rectangle blue", outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
