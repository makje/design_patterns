package mak1soft.composite;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

/**
 * Created by mak1 on 2017-03-20.
 */
public class CompositeTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void compositeTest() {
        Employee ceo = new Employee("mak1");

        Employee headSales = new Employee("head seles");

        Employee headMarketing = new Employee("head marketing");

        Employee selesExecutive1 = new Employee("seles executive1");
        Employee selesExecutive2 = new Employee("seles executive2");

        Employee clerk1 = new Employee("clerk1");
        Employee clerk2 = new Employee("clerk2");

        ceo.add(headSales);
        ceo.add(headMarketing);

        headSales.add(selesExecutive1);
        headSales.add(selesExecutive2);

        headMarketing.add(clerk1);
        headMarketing.add(clerk2);

        System.out.println(ceo);
        for (Employee headEmployee : ceo.getSubordinates()) {
            System.out.println(headEmployee);

            for (Employee employee : headEmployee.getSubordinates()) {
                System.out.println(employee);
            }
        }
        Assert.assertEquals("employee: mak1\r\n" +
                "employee: head seles\r\n" +
                "employee: seles executive1\r\n" +
                "employee: seles executive2\r\n" +
                "employee: head marketing\r\n" +
                "employee: clerk1\r\n" +
                "employee: clerk2\r\n", outContent.toString());
    }

    @Test
    public void removeEmployeeTest() {
        Employee headSales = new Employee("head seles");

        Employee selesExecutive1 = new Employee("seles executive1");
        Employee selesExecutive2 = new Employee("seles executive2");

        headSales.add(selesExecutive1);
        headSales.add(selesExecutive2);
        headSales.remove(selesExecutive1);

        for (Employee headEmployee : headSales.getSubordinates()) {
            System.out.println(headEmployee);
        }

        Assert.assertEquals("employee: seles executive2\r\n", outContent.toString());
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }
}
