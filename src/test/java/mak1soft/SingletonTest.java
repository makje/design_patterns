package mak1soft;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by mak1 on 2017-03-13.
 */
public class SingletonTest {

    @Test
    public void SingletonTest() {
        Singleton singleton1 = null;
        Assert.assertNull(singleton1);

        singleton1 = Singleton.getInstance();
        Assert.assertNotNull(singleton1);

        Singleton singleton2 = Singleton.getInstance();
        Assert.assertSame(singleton1, singleton2);

        Assert.assertEquals(singleton1, singleton2);
    }

}
